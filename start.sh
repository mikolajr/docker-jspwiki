#!/bin/sh

base_url=http://localhost:8080/wiki
local_port=8080
local_ajp_port=8009
data_dir=`pwd`/data
image=mikolajr/docker-jspwiki
uid=`id -u`
gid=`id -g`

docker pull ${image}

if [ ! -d "${data_dir}" ]; then
	mkdir -p "${data_dir}"
	echo "Starting JSPWiki to copy default set of pages"
	docker run --rm -d -e TOMCAT_UID=${uid} -e TOMCAT_GID=${gid} \
		--name jspwiki ${image}
	echo "Copying files...."
	docker cp jspwiki:/usr/local/tomcat/jspwiki/ "${data_dir}"
	echo "Closing container"
	docker stop jspwiki
	mv "${data_dir}"/jspwiki/* "${data_dir}"
	rm -rf "${data_dir}"/jspwiki
fi

if [ ! -d "${data_dir}" ]; then
	echo $data_dir not available
	exit 1
fi

docker run -d --rm -p ${local_port}:8080 -p ${local_ajp_port}:8009 \
	-e jspwiki_baseURL="${base_url}" \
	-e TOMCAT_UID=${uid} -e TOMCAT_GID=${gid} \
	--name jspwiki \
	--volume="${data_dir}:/usr/local/tomcat/jspwiki" ${image}
