# Supported tags and respective Dockerfile links

- [`latest`](https://bitbucket.org/mikolajr/docker-jspwiki/src/master/Dockerfile)

For more information about this image and its history, please see [its source repository](https://bitbucket.org/mikolajr/docker-jspwiki). 
This image is automagically built and updated via pull requests.
Only the most recent version of Tomcat is being maintained.

# What is JSPWiki?

[JSPWiki](https://jspwiki-wiki.apache.org/), Apache JSPWiki is a leading open source WikiWiki engine, feature-rich and built around standard JEE components (Java, servlets, JSP).

# Why another image?

This image makes it easier to run wiki in non standard setups. E.g. with custom UID and GID for tomcat process - beneficial for mounted
volumes (allows to have files created by container with correct owner).

As a bonus two nice start and stop scripts are provied. Download them from [bitbucket repository](https://bitbucket.org/mikolajr/docker-jspwiki).

Image exposes its HTTP and AJP ports.

# How to use this image

The easiest way:

```console
docker run -p 8080:8080 mikolajr/docker-jspwiki
```

But I suggest to download startup script and use it instead:

```console
./start.sh
```

You can alter start.sh script and provide base URL for your wiki and exposed http port as well. There might be some rare cases
when calculated uid or gid are already present in image (i.e. alpine linux distro) which may cause errors. In such case just provide
custom uid/gid (or do not use that feature at all - remove all '-e TOMCAT_UID=... -e TOMCAT_GID=...' parameters).

# Overriding defaults

This image provides a way to override default settings. 

Because this image is based on my [docker-tomcat](https://hub.docker.com/r/mikolajr/docker-tomcat/) image basic ways to manage tomcat container
are exactly the same.

# Contributing

You are invited to contribute new features, fixes, or updates, large or small.
